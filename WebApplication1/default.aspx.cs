﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb; // access bağlantısı için eklendi
using System.Data; // data işlemleri için eklendi

namespace WebApplication1
{
    
    public partial class _default : System.Web.UI.Page
    {
        OleDbConnection baglan;
        DataTable dt;
        OleDbDataAdapter da;
        OleDbCommand komut;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            baglan = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +Server.MapPath("App_Data\\ogrenci.accdb"));
            listele();
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            baglan.Open();
            int numara = Convert.ToInt32(txtNo.Text);
            string adSoyad = txtAdSoyad.Text;
            string sorgu = "INSERT INTO liste(Numara,AdSoyad) VALUES (" + numara + ",'" + adSoyad + "')";
            komut = new OleDbCommand(sorgu, baglan);
            //komut = new OleDbCommand("INSERT INTO liste(Numara,AdSoyad) VALUES (" + numara + ",'" + adSoyad + "')",baglan);
            komut.ExecuteNonQuery();
            baglan.Close();
            listele();
        }

        protected void txtSil_Click(object sender, EventArgs e)
        {
            baglan.Open();
            int numara = Convert.ToInt32(txtNo.Text);
            komut = new OleDbCommand("DELETE FROM liste WHERE Numara="+numara,baglan);
            komut.ExecuteNonQuery();
            baglan.Close();
            listele();
        }
        protected void listele()
        {
            string sorgu = "SELECT *FROM liste";
            da = new OleDbDataAdapter(sorgu, baglan);
            dt = new DataTable();
            baglan.Open();
            da.Fill(dt);
            baglan.Close();
            gvListe.DataSource = dt;
            gvListe.DataBind();
        }
    }
}