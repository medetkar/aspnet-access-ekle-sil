﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="WebApplication1._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style6 {
            width: 300px;
        }
        .auto-style7 {
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table class="auto-style6">
            <tr>
                <td colspan="2">Öğrenci Kayıt</td>
            </tr>
            <tr>
                <td class="auto-style7">Öğrenci No:</td>
                <td>
                    <asp:TextBox ID="txtNo" runat="server" Width="142px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style7">Ad Soyad:</td>
                <td>
                    <asp:TextBox ID="txtAdSoyad" runat="server" Width="142px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style7">
                    <asp:Button ID="btnEkle" runat="server" Text="Ekle" OnClick="btnEkle_Click" Width="124px" />
                </td>
                <td>
                    <asp:Button ID="txtSil" runat="server" Text="Sil" OnClick="txtSil_Click" Width="124px" />
                </td>
            </tr>
            <tr>
                <td class="auto-style7" colspan="2">
                    <asp:Label ID="lblBilgi" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
        </table>
        <asp:GridView ID="gvListe" runat="server">
        </asp:GridView>
    </form>
</body>
</html>
